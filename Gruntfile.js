const readdirp = require("readdirp");
const loadGruntTasks = require("load-grunt-tasks");
const {
  extname,
  dirname,
  posix: { relative }
} = require("path");

module.exports = grunt => {
  const PUBLIC_HOST = process.env.PUBLIC_HOST || "localhost";
  const BIND_IP = process.env.BIND_IP || "0.0.0.0";
  const BIND_PORT = 0 + process.env.BIND_PORT || 80;
  const LIVERELOAD_PORT = 0 + process.env.LIVERELOAD_PORT || 8899;
  const SLIDE_LEVEL = 0 + process.env.SLIDE_LEVEL || 2;
  const TOC_DEPTH = 0 + process.env.TOC_DEPTH || SLIDE_LEVEL;

  const sourcesGlob = ["**/*.{md,jpg,svg,png}", "!**/.*"];
  const revealJsSources = ["js/**/*.js", "css/**/*.css", "lib/**", "plugin/**"];

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),

    "http-server": {
      dev: {
        host: BIND_IP,
        port: BIND_PORT,
        root: "dist",
        runInBackground: true,
        openBrowser: true
      }
    },

    watcher: {
      configFiles: {
        files: ["Gruntfile.js"]
      },
      presentations: {
        files: sourcesGlob,
        tasks: [],
        options: {
          cwd: "src/.",
          debounceDelay: 1000,
          event: ["add", "change"],
          spawn: false,
          livereload: LIVERELOAD_PORT
        }
      },
      revealjs: {
        files: revealJsSources,
        task: ["build:revealjs"],
        options: {
          cwd: "node_modules/reveal.js/."
        }
      }
    },

    exec: {
      build: {
        cmd: (pres, env) =>
          [
            env === "dev" && "set -x ; ",
            `[ -s '../src/${pres}.conf' ] && . ../src/${pres}.conf ;`,
            "pandoc",
            "--verbose",
            "--from=markdown+smart",
            `../src/${pres}.md`,
            "--to=revealjs",
            `--output=./${pres}${env !== "prod" ? "-" + env : ""}.html`,
            "--standalone",
            "--toc",
            "--toc-depth=$TOC_DEPTH",
            "--slide-level=$SLIDE_LEVEL",
            "--variable=lang:fr-FR",
            `--variable=revealjs-url:${relative(dirname(pres), "./reveal.js")}`,
            "--variable=theme:irstea",
            "--variable=history:true",
            "--variable=navigationMode:linear",
            "--variable=fragmentInURL:true",
            "$PANDOC_ARGS",
            env === "dev" &&
              `--variable=header-includes:"<script src='//${PUBLIC_HOST}:${LIVERELOAD_PORT}/livereload.js'></script>"`
          ]
            .filter(v => !!v)
            .join(" "),
        cwd: "dist",
        stdout: true,
        stderr: true,
        options: {
          env: {
            TOC_DEPTH: "" + TOC_DEPTH,
            SLIDE_LEVEL: "" + SLIDE_LEVEL,
            PANDOC_ARGS: ""
          }
        }
      }
    },

    copy: {
      revealjs: {
        expand: true,
        cwd: "node_modules/reveal.js/.",
        src: revealJsSources,
        dest: "dist/reveal.js/"
      },
      sources: {
        expand: true,
        cwd: "src/",
        src: [...sourcesGlob, "!**/*.md"],
        dest: "dist/"
      }
    },

    clean: ["dist/**"]
  });

  loadGruntTasks(grunt, { pattern: ["grunt-*"] });

  grunt.registerTask("default", ["build"]);
  grunt.registerTask("serve", ["http-server", "build:dev", "watcher"]);

  grunt.registerTask("build:revealjs", ["copy:revealjs"]);
  grunt.registerTask("build:sources", ["copy:sources"]);

  grunt.registerTask("build", env =>
    grunt.task.run([
      "build:revealjs",
      "build:sources",
      `build:pres:${env || "prod"}`
    ])
  );

  const enqueueBuilds = (paths, env) =>
    grunt.task.run(
      paths
        .filter(name => name.endsWith(".md"))
        .map(
          name =>
            `exec:build:${name.substr(0, name.length - 3)}:${env || "prod"}`
        )
    );

  grunt.registerTask("build:pres", function(env) {
    const done = this.async();
    readdirp
      .promise("src/.", { fileFilter: "*.md" })
      .then(entries =>
        enqueueBuilds(
          entries.map(entry => entry.path),
          env
        )
      )
      .then(done, err => {
        grunt.log.error(err);
        done(false);
      });
  });

  grunt.event.removeAllListeners("chokidar");
  grunt.event.on("chokidar", function(_, filepath, target) {
    if (target === "presentations") {
      if (filepath.startsWith("src/")) {
        filepath = filepath.substr(4);
      }
      enqueueBuilds([filepath], "dev");
    }
  });
};
