FROM node:10 AS builder

RUN umask 0044

RUN mkdir -p /reveal_js

COPY package*.json Gruntfile.js /reveal_js/
COPY external /reveal_js/external/

WORKDIR /reveal_js

RUN npm install --only=prod

ARG PANDOC_VERSION=2.7.3

ADD https://github.com/jgm/pandoc/releases/download/${PANDOC_VERSION}/pandoc-${PANDOC_VERSION}-linux.tar.gz /tmp/pandoc.tar.gz

RUN tar xvfz /tmp/pandoc.tar.gz -C /usr/local --strip-components=1

RUN chmod -R +rX /reveal_js/node_modules/.bin/* /usr/local/bin/*

FROM node:10

COPY --from=builder /reveal_js /reveal_js/
COPY --from=builder /usr/local/bin/* /usr/local/bin/

RUN mkdir -p /src /dist
RUN ln -snf /src /reveal_js/src
RUN ln -snf /dist /reveal_js/dist

WORKDIR /reveal_js

RUN pandoc --version
RUN node_modules/.bin/grunt --version

EXPOSE 80/tcp 8899/tcp

ENTRYPOINT ["/reveal_js/node_modules/.bin/grunt"]

CMD ["serve"]
